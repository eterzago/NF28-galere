package nf28.td3.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import nf28.td3.Constantes;

import java.util.ArrayList;
import java.util.List;

public class Model {


    private ObservableList<Group> groupList;

    public Workspace getWorkspace() {
        return workspace;
    }

    private Workspace workspace;

    public Model()
    {
        groupList = FXCollections.observableArrayList();
        workspace = new Workspace();
    }

    // Adds a new default group
    public void addGroup()
    {
        Group newGroup = new Group(Constantes.GROUP_DEFAULT_NAME);
        groupList.add(newGroup);
    }

    public ObservableList<Group> getGroupList() {
        return groupList;
    }


    public void setGroupList(ArrayList<Group> groupListParam) {
        for (Group g:
             this.groupList) {
            this.groupList.remove(g);
        }

        this.groupList.addAll(groupListParam);

        //this.groupList = FXCollections.observableArrayList(groupList);
        System.out.println("this : "+this.groupList);
    }

    public void setGroupList(ObservableList<Group> groupList) {
        this.groupList = groupList;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }
}
