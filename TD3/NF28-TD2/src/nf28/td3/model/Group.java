package nf28.td3.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class Group {
    private ObservableList<Contact> contactList;
    private StringProperty groupName;

    public Group()
    {
        this("DefaultName");
    }
    public Group(String name)
    {
        contactList = FXCollections.observableArrayList();
        groupName = new SimpleStringProperty(null, "groupName", name);
    }

    public void addContact(Contact c)
    {
        contactList.add(c);
    }

    @Override
    public String toString()
    {
        return groupName.getValue();
    }

    public String getGroupName() {
        return groupName.get();
    }
    public StringProperty groupNameProperty() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName.set(groupName);
    }

    public ObservableList<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<Contact> contactListParam) {
        this.contactList.clear();
        this.contactList.addAll(contactListParam);
    }
}
