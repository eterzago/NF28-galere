package nf28.td3.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Workspace {
    private ArrayList<Group> groupList;

    public Workspace()
    {

    }

    public ArrayList<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList<Group> groupList) {
        this.groupList = groupList;
    }

    public ArrayList<Group> fromFile(File f) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        Workspace w =  mapper.readValue(f, Workspace.class);
        this.groupList = w.groupList;

        return this.groupList;
    }

    public void save (File f) throws IOException
    {
        System.out.println("Saving : "+f.getName());

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(f, this);
    }

    public void addGroups(ObservableList<Group> groups)
    {
        groupList = new ArrayList<>(groups);
    }

}
