package nf28.td3.model;

public enum ErrorType {
    NO_ERROR,
    WRONG_VALUE,
    FIELD_EMPTY
}
