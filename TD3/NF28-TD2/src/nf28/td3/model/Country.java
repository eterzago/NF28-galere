package nf28.td3.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Collectors;

public class Country {

    public static ObservableList getCountries()
    {
        ObservableList<String> countryNames;
        String[] locales = Locale.getISOCountries();

        countryNames = FXCollections.observableArrayList(Arrays.stream(locales)
                .map(countryCode -> (new Locale("", countryCode))
                    .getDisplayCountry()).sorted()
                .collect(Collectors.toList()));

        return countryNames;
    }



}
