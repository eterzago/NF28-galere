package nf28.td3.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Contact {
    private StringProperty prenom;
    private StringProperty nom;
    private ObjectProperty<Adresse> adresse;
    private ObjectProperty<LocalDate> naissance;
    private Sexe sexe;

    public Contact()
    {
        prenom = new SimpleStringProperty(null,"prenom","");
        nom = new SimpleStringProperty(null,"nom","");
        adresse = new SimpleObjectProperty<>(new Adresse());
        naissance = new SimpleObjectProperty<>(null);
        sexe = Sexe.M; // Default value
    }
    public Contact(Contact c)
    {
        prenom = new SimpleStringProperty(null, "prenom", c.getPrenom());
        nom = new SimpleStringProperty(null, "nom", c.getNom());
        adresse = new SimpleObjectProperty<>(new Adresse());
        adresse.getValue().setPays(c.getAdresse().getPays());
        adresse.getValue().setVille(c.getAdresse().getVille());
        adresse.getValue().setCodePostal(c.getAdresse().getCodePostal());
        adresse.getValue().setLibelle(c.getAdresse().getLibelle());
        naissance = new SimpleObjectProperty<>(null);
        naissance.setValue(c.naissanceProperty().getValue());
        sexe = c.getSexe();
    }

    public void reinitContact(Contact c)
    {
        setPrenom(c.getPrenom());
        setNom(c.getNom());
        setSexe(c.getSexe());
        adresse.getValue().setCodePostal(c.getAdresse().getCodePostal());
        adresse.getValue().setVille(c.getAdresse().getVille());
        adresse.getValue().setLibelle(c.getAdresse().getLibelle());
        adresse.getValue().setPays(c.getAdresse().getPays());
        setNaissance(c.getNaissance());
    }

    public ErrorType checkNom()
    {
        // If field is empty
        if(nom.getValue().isEmpty()) {
            return ErrorType.FIELD_EMPTY;
        }
        // If field is wrongly filled
        // else if(regex(nom.getValue())) return ErrorType.WRONG_VALUE;
        else return ErrorType.NO_ERROR;
    }
    public ErrorType checkPrenom()
    {
        // If field is empty
        if(prenom.getValue().isEmpty()) {
            return ErrorType.FIELD_EMPTY;
        }
        else return ErrorType.NO_ERROR;
    }
    public ErrorType checkPays()
    {
        // If field is empty
        System.out.println("pays "+adresse.getValue().getPays());
        System.out.println("pays "+adresse.getValue().getPays().toString());
        if(adresse.getValue().getPays().toString().isEmpty()) {
            return ErrorType.FIELD_EMPTY;
        }
        else return ErrorType.NO_ERROR;
    }
    public ErrorType checkVille()
    {
        // If field is empty
        if(adresse.getValue().getVille().isEmpty()) {
            return ErrorType.FIELD_EMPTY;
        }
        else return ErrorType.NO_ERROR;
    }
    public ErrorType checkCodePostal()
    {
        // If field is empty
        if(adresse.getValue().getCodePostal().isEmpty()) {
            return ErrorType.FIELD_EMPTY;
        }
        else return ErrorType.NO_ERROR;
    }
    public ErrorType checkLibelle()
    {
        // If field is empty
        if(adresse.getValue().getLibelle().isEmpty()) {
            return ErrorType.FIELD_EMPTY;
        }
        else return ErrorType.NO_ERROR;
    }
    public ErrorType checkDate()
    {
        // If field is empty
        if(naissance.getValue() == null) {
            return ErrorType.FIELD_EMPTY;
        }
        else return ErrorType.NO_ERROR;
    }


    public String getPrenom() {
        return prenom.get();
    }

    public StringProperty prenomProperty() {
        return prenom;
    }

    public String getNom() {
        return nom.get();
    }

    public StringProperty nomProperty() {
        return nom;
    }

    public Adresse getAdresse() {
        return adresse.get();
    }

    public ObjectProperty<Adresse> adresseProperty() {
        return adresse;
    }


    public ObjectProperty<LocalDate> naissanceProperty() {
        return naissance;
    }

    public String getNaissance() {
        return naissance.get().getDayOfMonth()+"/"+naissance.get().getMonthValue()+"/"+naissance.get().getYear();
    }



    public Sexe getSexe() {
        return sexe;
    }
    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public void setPrenom(String prenom) {
        this.prenom.set(prenom);
    }

    public void setNom(String nom) {
        this.nom.set(nom);
    }

    public void setAdresse(Adresse adresse) {
        this.adresse.set(adresse);
    }

    public void setNaissance(String naissanceParam)
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
        this.naissance.set(LocalDate.parse(naissanceParam, dtf));
    }

    @Override
    public String toString()
    {
        return this.prenom.get() + " " + this.nom.get();
    }
}
