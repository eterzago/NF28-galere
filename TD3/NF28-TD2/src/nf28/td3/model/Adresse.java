package nf28.td3.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Adresse {
    private StringProperty libelle;
    private StringProperty codePostal;
    private StringProperty ville;
    private StringProperty pays;

    public Adresse()
    {
        libelle = new SimpleStringProperty(null,"libelle","");
        codePostal = new SimpleStringProperty(null,"codePostal","");
        ville = new SimpleStringProperty(null,"ville","");
        pays = new SimpleStringProperty(null,"pays","");
    }

    public Adresse(StringProperty unLibelle, StringProperty unCodePostal,
                    StringProperty uneVille, StringProperty unPays)
    {
        libelle = unLibelle;
        codePostal = unCodePostal;
        ville = uneVille;
        pays = unPays;
    }

    public String getLibelle() {
        return libelle.get();
    }

    public StringProperty libelleProperty() {
        return libelle;
    }

    public String getCodePostal() {
        return codePostal.get();
    }

    public StringProperty codePostalProperty() {
        return codePostal;
    }

    public String getVille() {
        return ville.get();
    }

    public StringProperty villeProperty() {
        return ville;
    }

    public String getPays() {
        return pays.get();
    }

    public StringProperty paysProperty() {
        return pays;
    }

    public void setLibelle(String libelle) {
        this.libelle.set(libelle);
    }

    public void setCodePostal(String codePostal) {
        this.codePostal.set(codePostal);
    }

    public void setVille(String ville) {
        this.ville.set(ville);
    }

    public void setPays(String pays) {
        this.pays.set(pays);
    }


}
