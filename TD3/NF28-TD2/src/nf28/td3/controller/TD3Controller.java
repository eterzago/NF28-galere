package nf28.td3.controller;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import nf28.td3.Constantes;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import nf28.td3.model.*;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.toList;


public class TD3Controller {

    @FXML
    TreeView<Object> contactTreeView;
    @FXML
    VBox contactPane;
    @FXML
    Button addButton;
    @FXML
    Button removeButton;

    @FXML
    Button valider;
    @FXML
    TextField prenom;
    @FXML
    TextField nom;
    @FXML
    TextField voie;
    @FXML
    TextField codePostal;
    @FXML
    TextField ville;
    @FXML
    ComboBox pays;
    @FXML
    DatePicker naissance;
    @FXML
    ToggleGroup genderGroup;
    @FXML
    RadioButton genreM;
    @FXML
    RadioButton genreF;

    @FXML
    MenuItem fileOpen;
    @FXML
    MenuItem fileSave;


    private Model model;
    private Contact editingContact;
    private Contact originalContact;


    // Contact tree icons
    private final Image groupIcon = new Image("file:"+Constantes.ICON_GROUP);
    private final Image contactIcon = new Image("file:"+Constantes.ICON_CONTACT);


    public TD3Controller()
    {
        this.model = new Model();
        this.editingContact = new Contact();
        this.originalContact = new Contact();
    }


    @FXML
    public void initialize()
    {
        fileOpen.setOnAction(actionEvent -> {
            System.out.println("Open");
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            File f = fileChooser.showOpenDialog(new Stage());
            try {
                this.model.setGroupList(model.getWorkspace().fromFile(f));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println(this.model.getGroupList()); // TODO REPRENDRE ICI ; CA CA MARCHE DONC PK LE RESTE NE MARCHE PAS ?


        });
        fileSave.setOnAction(actionEvent -> {
            System.out.println("Save");
            this.model.getWorkspace().addGroups(model.getGroupList());
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save Resource File");
            File f = fileChooser.showSaveDialog(new Stage());
            try {
                this.model.getWorkspace().save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        // Initializing treeview
        initializeTreeView();

        // Contact form is not visible for the moment
        contactPane.visibleProperty().set(false);

        // Adding and removing contacts
        addButton.setOnMouseClicked((e)->
        {
            // If the root was selected, adds a new group
            if(contactTreeView.getSelectionModel().getSelectedItem() == contactTreeView.getRoot())
            {
                System.out.println("Root was selected !");
                model.addGroup();
            }
            // If a group was selected, adds a new Contact in selected group
            else if(contactTreeView.getSelectionModel().getSelectedItem().getValue() instanceof Group)
            {
                System.out.println("Group was selected !");
                contactPane.visibleProperty().set(true);
            }
            else System.out.println("Nothing was selected !");

        });

        // Listening for change on model's grouplist, to change the view
        model.getGroupList().addListener((ListChangeListener<Group>) change -> {
            while(change.next()) {
                // If a new Group was added
                if (change.wasAdded())
                {
                    for (Group addGroup : change.getAddedSubList()) {
                        TreeItem<Object> newGroup = new TreeItem<>(addGroup, new ImageView(groupIcon));
                        contactTreeView.getRoot().getChildren().add(newGroup);
                        System.out.println("Added new group");
                    }
                }
            }
        });

        // Contact form
        pays.setItems(Country.getCountries());
        pays.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            editingContact.getAdresse().paysProperty().setValue(newValue.toString());
        });
        prenom.textProperty().bindBidirectional(editingContact.prenomProperty());
        nom.textProperty().bindBidirectional(editingContact.nomProperty());
        voie.textProperty().bindBidirectional(editingContact.getAdresse().libelleProperty());
        codePostal.textProperty().bindBidirectional(editingContact.getAdresse().codePostalProperty());
        ville.textProperty().bindBidirectional(editingContact.getAdresse().villeProperty());
        genderGroup.selectedToggleProperty().addListener((observable,oldValue,newValue) ->
        {
            if(((RadioButton)newValue).getText().equals(Constantes.GENDER_MALE))
            {
                editingContact.setSexe(Sexe.M);
            }
            else if (((RadioButton)newValue).getText().equals(Constantes.GENDER_FEMALE))
            {
                editingContact.setSexe(Sexe.F);
            }

        });

        naissance.valueProperty().bindBidirectional(editingContact.naissanceProperty());

        valider.setOnMouseClicked((e)->{
            if(checkFields())
            {
                Contact newContact = new Contact(editingContact);


                if(contactTreeView.getSelectionModel().getSelectedItem().getValue() instanceof Contact)
                {
                    originalContact.reinitContact(editingContact);

                    // Visual refresh
                    contactTreeView.getRoot().setExpanded(false);
                    contactTreeView.getRoot().setExpanded(true);
                }
                else if(contactTreeView.getSelectionModel().getSelectedItem().getValue() instanceof Group)
                {
                    // Adding new contact in the model
                    ((Group)contactTreeView.getSelectionModel().getSelectedItem().getValue()).addContact(newContact);

                    // Adding new item in the view
                    contactTreeView.getSelectionModel().getSelectedItem().getChildren().add(new TreeItem<Object>(newContact, new ImageView(contactIcon)));
                    contactTreeView.getSelectionModel().getSelectedItem().setExpanded(true);
                }

                contactPane.visibleProperty().set(false);

                resetEditingContact();
            }
        });

    }

    public void resetEditingContact()
    {
        this.editingContact.nomProperty().setValue("");
        this.editingContact.prenomProperty().setValue("");
        this.editingContact.adresseProperty().getValue().setLibelle("");
        this.editingContact.adresseProperty().getValue().setVille("");
        this.editingContact.adresseProperty().getValue().setPays("");
        this.editingContact.adresseProperty().getValue().setCodePostal("");
        this.editingContact.naissanceProperty().setValue(null);
    }


    private boolean checkFields()
    {
        boolean bob = true;

        if(editingContact.checkNom() == ErrorType.FIELD_EMPTY)
        {
            nom.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_WARNING);
            nom.setTooltip(new Tooltip(Constantes.MISSING_NAME));
            bob = false;
        }
        else {
            nom.setTooltip(null);
            nom.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_NOWARNING);
        }
        if(editingContact.checkPrenom() == ErrorType.FIELD_EMPTY)
        {
            prenom.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_WARNING);
            prenom.setTooltip(new Tooltip(Constantes.MISSING_FIRSTNAME));
            bob = false;
        }
        else {
            prenom.setTooltip(null);
            prenom.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_NOWARNING);
        }
        if(editingContact.checkLibelle() == ErrorType.FIELD_EMPTY)
        {
            voie.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_WARNING);
            voie.setTooltip(new Tooltip(Constantes.MISSING_STREET));
            bob = false;
        }
        else {
            voie.setTooltip(null);
            voie.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_NOWARNING);
        }
        if(editingContact.checkCodePostal() == ErrorType.FIELD_EMPTY)
        {
            codePostal.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_WARNING);
            codePostal.setTooltip(new Tooltip(Constantes.MISSING_ZIPCODE));
            bob = false;
        }
        else {
            codePostal.setTooltip(null);
            codePostal.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_NOWARNING);
        }
        if(editingContact.checkVille() == ErrorType.FIELD_EMPTY)
        {
            ville.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_WARNING);
            ville.setTooltip(new Tooltip(Constantes.MISSING_CITY));
            bob = false;
        }
        else {
            ville.setTooltip(null);
            ville.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_NOWARNING);
        }
        if(editingContact.checkPays() == ErrorType.FIELD_EMPTY)
        {
            pays.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_WARNING);
            pays.setTooltip(new Tooltip(Constantes.MISSING_COUNTRY));
            bob = false;
        }
        else {
            pays.setTooltip(null);
            pays.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_NOWARNING);
        }
        if(editingContact.checkDate() == ErrorType.FIELD_EMPTY)
        {
            naissance.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_WARNING);
            naissance.setTooltip(new Tooltip(Constantes.MISSING_BIRTHDATE));
            bob = false;
        }
        else {
            naissance.setTooltip(null);
            naissance.setStyle(Constantes.CSS_BORDER_COLOR+":"+Constantes.CSS_COLOR_NOWARNING);
        }
        return bob;
    }

    private void initializeTreeView()
    {
        TreeItem<Object> root = new TreeItem<>(Constantes.TREEVIEW_ROOT);
        contactTreeView.setRoot(root);
        root.setExpanded(true);

        // Setting cell factory
        contactTreeView.setCellFactory(param -> new TextFieldTreeCellImpl());
        contactTreeView.setEditable(true);
    }


    private final class TextFieldTreeCellImpl extends TreeCell<Object> {

        private TextField textField;

        public TextFieldTreeCellImpl() { }

        @Override
        public void startEdit()
        {
            if(getTreeItem().getValue() instanceof Group)
            {
                System.out.println("Item selected is a group ! Editing group...");
                contactPane.visibleProperty().set(false);
                super.startEdit();

                if (textField == null) {
                    createTextField();
                }
                setText(null);
                setGraphic(textField);
                textField.selectAll();
            }
             if (getItem() instanceof Contact)
            {
                System.out.print("Item selected is a contact ! Opening contact edit form...");
                System.out.println(" Contact : "+((Contact) getTreeItem().getValue()).getPrenom()+" "+((Contact) getTreeItem().getValue()).getNom());
                contactPane.visibleProperty().set(true);
                originalContact = ((Contact) getTreeItem().getValue());
                editingContact.reinitContact(originalContact);
            }
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText((String) getItem());
            setGraphic(getTreeItem().getGraphic());
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setOnKeyReleased(t -> {
                if (t.getCode() == KeyCode.ENTER) {
                    ((Group)getItem()).setGroupName(textField.getText());
                    commitEdit(getItem());
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            });
        }

        @Override
        public void updateItem(Object item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setText(null);
                    setGraphic(textField);
                } else {
                    setText(getString());
                    setGraphic(getTreeItem().getGraphic());
                }
            }
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }


    /*public List<PieChart.Data> contactPieChartData()
    {
        List<PieChart.Data> ldata = model.groupsProperty().stream().map(group->new PieChart.Data(group.getName(), group.contactSize())).collect(toList());
        return ldata;
    }

    public XYChart.Series<String,Number> citiesSeriesData()
    {
        XYChart.Series<String,Number> citySerie = new XYChart.Series<String,Number>();
        model.groupsProperty().stream().flatMap(group->group.getContacts().stream())
                .collect(groupingBy(contact->contact.getAddress().getCity(), counting()))
                .forEach((city,nb) -> citySerie.getData().add().add(new XYChart.Data<String,Number>(city,nb)));
        return citySerie;
    }*/

}
