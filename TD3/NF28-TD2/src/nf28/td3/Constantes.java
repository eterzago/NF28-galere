package nf28.td3;

public class Constantes {
    public static final String TREEVIEW_ROOT = "Fiche de contacts";
    public static final String GROUP_DEFAULT_NAME = "Nouveau groupe";

    public static final String GENDER_MALE = "M";
    public static final String GENDER_FEMALE = "F";

    public static final String CSS_BORDER_COLOR = "-fx-border-color";
    public static final String CSS_COLOR_WARNING = "red";
    public static final String CSS_COLOR_NOWARNING = "transparent";

    public static final String NOTHING_MISSING = "";
    public static final String MISSING_NAME = "Manque nom !";
    public static final String MISSING_FIRSTNAME = "Manque prénom !";
    public static final String MISSING_STREET = "Manque libellé de la voie !";
    public static final String MISSING_ZIPCODE = "Manque code postal !";
    public static final String MISSING_CITY = "Manque ville !";
    public static final String MISSING_COUNTRY = "Manque pays !";
    public static final String MISSING_BIRTHDATE = "Manque date de naissance !";

    // Graphical resources
    public static final String ICON_CONTACT = "./img/contact.png";
    public static final String ICON_GROUP = "./img/group.png";

}
